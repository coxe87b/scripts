echo
echo Running site backup script...
echo
sleep 1s
echo Changing to web root /var/www
echo
cd /var/www
sleep 1s
today=$(date +%Y-%m-%d_%H:%M)
filename="html-backup-$today"
echo Creating directory $today
echo Copying files
sleep 2s
sudo cp -Prv html $filename
echo
echo Backup complete.
sleep 1s
