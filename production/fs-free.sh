#!/usr/bin/env bash
#
# Script to display filesystem usage snapshot with usage warnings
# By: Ewan Cox
#
# Declare mountpoint variables - *** CHANGE THESE LOCATIONS TO SUIT YOUR MOUNTPOINTS ***
# INFO: Add more mountpoints inside the 'mountpoint' array inside the parentheses and separated by spaces
# INFO: Do not have spaces in an individual label name as it will throw out the formatting of column
#
declare -a mountpoint=("/home" "/mnt/hd0" "/mnt/nv2" "/mnt/nv0")
declare -a label=("Root-FS" "Filesystem")
#
#

drive_list(){
    df -h / | awk 'NR==2 {print "'"${label[0]}"' [" $1 "] mounted on [/] " $5 " in use, " $4 " available"}'

    for j in ${mountpoint[@]}; do
        df -h $j | awk 'NR==2 {print "'""${label[1]}""' [" $1 "] mounted on ['$j'] " $5 " in use, " $4 " available"}'
    done
}
space_check(){
    for i in ${mountpoint[@]}; do
        if [[ $(df -h $i | awk 'NR==2 {print $5}' | sed 's/[^0-9]*//g') -gt 85 ]]; then
            echo -e "\e[0;33m\t\tWarning: There is less than 15% available space on ${i}! \e[0m"
        fi
    done
}
critical_space_check(){
    for i in ${mountpoint[@]}; do
        if [[ $(df -h $i | awk 'NR==2 {print $5}' | sed 's/[^0-9]*//g') -gt 95 ]]; then
            echo -e "\e[0;31m\t\tCritical: There is less than 5% available space on ${i}! \e[0m"
        fi
    done
}

echo

# Function to print stats about each mountpoint
drive_list | column -t
echo

# Check root partition for less than 20% free space
if [[ $(df -h / | awk 'NR==2 {print $5}' | sed 's/[^0-9]*//g') -gt 81 ]]; then
    echo -e "\e[0;31m\t\t*** Warning: Less than 20% free in root partition! ***\e[0m"
    echo
fi

# Function to check each drive for less than 15% free space
space_check

# Function to alert of critical less than 5% free space
critical_space_check
