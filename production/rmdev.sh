#!/bin/bash

# Device offline script made by Ewan Cox.
#
# Todo: add function to list block devices at end.
#
# Declare variables
#
rmdev=$1
rootdev=`df / | awk 'NR==2 {print substr($1, 6, 3)}'`
insthdp=

# Declare functions
#

# Main script
#
if [[ `id -u` -ne 0 ]]; then
    echo
    echo "Error 101: Please run this script with root or sudo."
    echo
    exit 101
fi

if ! command -v hdparm; then
    echo
    echo "Error 102: This script requires the [ hdparm ] package to be installed."
    echo
    read -p "  Do you want to try to install it now? [y/N]: " insthdp
    echo
    if [ $insthdp = "y" ] || [ $insthdp = "Y" ]; then
        echo "Install of [ hdparm ] confirmed."
        echo -n "Detecting package manager... "
        if grep -i "arch"; then
            echo "Done."
            echo "Arch based distribution detected."
            echo "Installing [ hdparm ] with pacman..."
            sudo pacman -Sy --noconfirm hdparm || echo "Error 103: Error when trying to install package, see pacman output."; exit 103
        elif grep -i "debian" || grep -i "ubuntu"; then
            echo "Done."
            echo "Debian/Ubuntu based distribution detected."
            echo "Installing [ hdparm ] with apt..."
            sudo apt update && sudo apt install hdparm || echo "Error 104: Error when trying to install package, see apt output."; exit 104
        else
            echo "Failed."
            echo "Unable to auto-install [ hdparm ]. Please manually install and try again."
            echo
            exit 102
        fi
    fi
fi

while [[ -z $rmdev ]]
do
    echo
    read -p "  Please enter the USB/SATA device (not partition) to remove (eg. sda, sdb, etc.): " rmdev
    echo
done

if [[ $rmdev =~ "nvme" ]]
then
    echo
    echo "Error 105: Cannot hot-remove NVMe device!"
    echo "Please power off your system to do this."
    echo
    exit 105
fi

if [[ ! $rmdev = "sd"* ]]
then
    echo
    echo "Error 106: You must enter device in format (sdX), where X = letter corresponding to device."
    echo
    exit 106
fi

if [ $rmdev = $rootdev ]
then
    echo
    echo "Error 107: Cannot remove root filesystem!"
    echo
    exit 107
fi

if [ -e /sys/block/$rmdev ]; then
    echo
    echo -n "Issuing sleep command... "
    hdparm -Y /dev/$rmdev > /dev/null || hd1="fail"
    if [[ $hd1 = "fail" ]]; then
        echo "Failed."
        echo "Error 109: Problem with [ hdparm ]."
        exit 109
    fi
    echo "Done."
    echo -n "Taking device [ $rmdev ] offline... "
    echo offline > /sys/block/${rmdev}/device/state || hd2="fail"
    if [[ $hd2 = "fail" ]]; then
        echo "Failed."
        echo "Error 110: Problem issuing offline command."
        exit 110
    fi
    echo "Done."
    echo -n "Removing system device entry for: [ $rmdev ]... "
    echo 1 > /sys/block/${rmdev}/device/delete || hd3="fail"
    if [[ $hd3 = "fail" ]]; then
        echo "Failed."
        echo "Error 111: Problem deleting device record."
        exit 111
    fi
    echo "Done."
    echo "Device removed successfully, you may now unplug it from the system."
    echo
else
    echo
    echo "Error 108: Device $rmdev not found!"
    echo
    exit 108
fi
