#!/bin/sh

# Hash checker script by Ewan Cox.

# User choice for hash type

# Add function to compare pasted value with pasted value

echo "Check sha256sum integrity script:"
echo

if [ $# -eq 1 ]
then
	filename1=$1
	echo "Filename selected: [ $filename1 ]"
    echo
else
	read -p "Please enter filename within current directory to compare: " filename1
    echo
fi

if [ ! -e $filename1 ]; then
    echo "File does not exist"
    exit
fi

while [ -z $checksum1 ]
do
	read -p "Please enter/paste checksum hash: " checksum1
done

echo
echo "Checking $filename1 against checksum $checksum1, please wait..."
echo

len=`expr length "$checksum1"`

if [ $len = 64 ]
then
    echo $checksum1 $filename1 | sha256sum -c
else
    echo "Bad checksum length: $len"
fi
